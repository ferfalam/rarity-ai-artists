import React, { useState } from 'react';

const IncomingNFts = () => {
    return (
        <div className="bg-white">
            <section className="py-12 sm:py-16 lg:pb-20">
                <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
                    <div className="grid grid-cols-3 gap-5 mx-auto sm:max-w-md">
                        <img className="rounded-xl" src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-coming-soon/1/image-1.png" alt="" />
                        <img className="transform -rotate-2 rounded-xl" src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-coming-soon/1/image-2.png" alt="" />
                        <img className="transform rounded-xl rotate-2" src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-coming-soon/1/image-3.png" alt="" />
                    </div>

                    <div className="max-w-md mx-auto mt-8 text-center">
                        <h1 className="text-3xl font-bold text-gray-900 sm:text-4xl lg:text-5xl">Apes are coming</h1>
                        <p className="mt-4 text-base font-medium text-gray-500 lg:text-lg">A collection of 100 funny ape NFTs-unique digital collectibles living on the Ethereum blockchain will be available.</p>
                    </div>

                    <div className="max-w-sm mx-auto mt-10 overflow-hidden text-center bg-gray-900 rounded-xl">
                        <div className="p-6">
                            <div className="flex items-center justify-between px-1 space-x-3 lg:space-x-6">
                                <div>
                                    <p className="text-4xl font-bold text-white">37</p>
                                    <p className="mt-1 text-sm font-medium text-gray-400">Days</p>
                                </div>

                                <div>
                                    <p className="text-2xl font-bold text-white animate-pulse">:</p>
                                </div>

                                <div>
                                    <p className="text-4xl font-bold text-white">14</p>
                                    <p className="mt-1 text-sm font-medium text-gray-400">Hours</p>
                                </div>

                                <div>
                                    <p className="text-2xl font-bold text-white animate-pulse">:</p>
                                </div>

                                <div>
                                    <p className="text-4xl font-bold text-white">45</p>
                                    <p className="mt-1 text-sm font-medium text-gray-400">Mins</p>
                                </div>

                                <div>
                                    <p className="text-2xl font-bold text-white animate-pulse">:</p>
                                </div>

                                <div>
                                    <p className="text-4xl font-bold text-white">17</p>
                                    <p className="mt-1 text-sm font-medium text-gray-400">Secs</p>
                                </div>
                            </div>

                            <div className="mt-5">
                                <a
                                    href="#"
                                    title=""
                                    className="inline-flex items-center justify-center w-full px-6 py-4 text-xs font-bold tracking-widest text-white uppercase transition-all duration-200 border border-transparent rounded-lg bg-rose-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-rose-600 hover:bg-rose-600"
                                    role="button"
                                >
                                    Join Discord
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div >

    )
}
export default IncomingNFts;