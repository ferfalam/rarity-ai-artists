import React, { useEffect, useRef, useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { useDispatch, useSelector } from 'react-redux';
import { REQUEST } from '../store/reducers/userReducer';
import { api } from '../store/api';

const ConnectWallet = () => {
    const user = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const popup = useRef();
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState({
        type: '',
        text: ''
    });
    const [loading, setLoading] = useState(false);
    const [unavailableChains, setUnavailableChains] = useState([]);

    useEffect(() => {
        // check if metaMask is installed
        if (!window.ethereum) {
            setUnavailableChains([...unavailableChains, 'Ethereum']);
        }

        // check if url has error
        const url = new URL(window.location.href);
        const error = url.searchParams.get('error');
        if (error && error === 'access_denied') {
            setMessage({ type: 'error', text: 'It seems like you rejected the connection request. Please try again.' });
        }
    }, []);

    const close = () => {
        dispatch(REQUEST(null));
    };

    useEffect(() => {
        if (user?.request === 'signIn') setOpen(true);
    }, [user]);

    const onClick = (e) => {
        if (!e.target.closest('#popup_wallet')) {
            setOpen(false);
            if (open) close();
        }
    };

    useEffect(() => {
        document.addEventListener('click', onClick, true);
        return () => {
            document.removeEventListener('click', onClick, true);
        };
    });

    const signInWithDiscord = async (e) => {
        e.preventDefault();

        const { data, error } = await api.auth.signInWithOAuth({
            provider: 'discord',
        });

        if (data) setLoading(true);
    };

    const signInWithMetamask = (e) => {
        e.preventDefault();
        if (unavailableChains.indexOf('Ethereum') === -1) {
            const { ethereum } = window;

            setLoading(true);

            if (ethereum) {
                ethereum
                    .enable()
                    .then(() => {
                        ethereum
                            .request({ method: 'eth_accounts' })
                            .then((accounts) => {
                                if (accounts.length > 0) {
                                    setMessage('');
                                    dispatch(signUp(accounts[0]));
                                }
                            })
                            .catch((error) => {
                                if (message?.toLowerCase() !== error.message?.toLowerCase())
                                    setMessage('');
                                setLoading(false);
                                setMessage(error.message);
                            });
                    })
                    .catch((error) => {
                        if (error?.code === -32002) {
                            setMessage(
                                'You already requested to sign in, please check your metamask'
                            );
                        } else {
                            if (message?.toLowerCase() !== error.message?.toLowerCase())
                                setMessage('');
                            setLoading(false);
                        }
                    });
            }
        } else {
            setMessage(
                'MetaMask is not installed. <a className="link button-link" href="https://metamask.io/">Install it</a><br/> If you already have it installed, please refresh the page.'
            );
        }
    };

    return (
        <AnimatePresence>
            {open && (
                <>
                    <motion.div
                        initial={{
                            opacity: 0,
                        }}
                        animate={{
                            opacity: 1,
                            transition: {
                                duration: 0.25,
                                ease: 'easeInOut',
                            },
                        }}
                        exit={{
                            opacity: 0,
                            transition: {
                                duration: 0.25,
                            },
                        }}
                        className="fixed top-0 px-5 z-50 h-screen w-full justify-center items-center text-center modal_background "
                        onClick={onClick}
                    >
                        <motion.div
                            initial={{
                                scale: 0.9,
                            }}
                            animate={{
                                scale: 1,
                                transition: {
                                    duration: 0.25,
                                    ease: 'easeInOut',
                                },
                            }}
                            exit={{
                                scale: 0,
                                transition: {
                                    delay: 0.25,
                                },
                            }}
                            id="popup_wallet"
                            ref={popup}
                            className={`w-full max-w-xl mx-auto top-44 sm:px-14 pb-10 pt-8 overflow-hidden bg-white rounded-2xl flex flex-col items-center transition-all relative
                            ${!open ? `-translate-y-4 opacity-0` : `translate-y-0 opacity-100`}`}
                        >
                            <motion.div
                                initial={{
                                    y: 25,
                                    opacity: 0,
                                }}
                                animate={{
                                    y: 0,
                                    opacity: 1,
                                    transition: {
                                        delay: 0.25,
                                        duration: 0.25,
                                    },
                                }}
                                exit={{
                                    y: 25,
                                    opacity: 0,
                                    transition: {
                                        duration: 0.25,
                                    },
                                }}
                            >
                                <div className="p-6 sm:p-8">
                                    <div className="flex items-start justify-between">
                                        <div className="flex-1">
                                            <p className="text-xl font-bold text-gray-900">
                                                Connect your wallet
                                            </p>
                                            {!user?.currentUser && <p className="mt-1 text-base font-medium text-gray-500">
                                                You need to sign in with your discord account to continue
                                            </p>}
                                        </div>

                                        <div>
                                            <button type="button"
                                                onClick={() => {
                                                    setOpen(false);
                                                    if (open) close();
                                                }}
                                                className="p-1.5 text-gray-400 transition-all duration-200 bg-white border border-gray-200 rounded-full hover:bg-gray-100 hover:text-gray-700 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-offset-2">
                                                <span className="sr-only">
                                                    Close
                                                </span>
                                                <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                    stroke="currentColor" aria-hidden="true">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12">
                                                    </path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>

                                    <hr className="mt-8 border-gray-200" />

                                    {user?.currentUser && <div>
                                        <div className="mt-6">
                                            <p className="text-base font-bold text-gray-900">
                                                Choose Blockchain
                                            </p>
                                            <div className="grid grid-cols-1 gap-4 mt-5">
                                                <div
                                                    className="relative overflow-hidden transition-all duration-200 bg-white border border-gray-500 cursor-pointer rounded-xl">
                                                    <div className="px-4 py-5">
                                                        <div className="flex items-start">
                                                            <img className="object-contain w-8 h-8 shrink-0" src="/assets/polygon.png" alt="" />
                                                            <div className="ml-4">
                                                                <p className="text-sm font-bold text-gray-900 text-left">
                                                                    Polygon
                                                                </p>
                                                                <p className="mt-1 text-sm font-medium text-gray-500">
                                                                    Because we are a community based collection
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="mt-6">
                                            <p className="text-base font-bold text-gray-900">
                                                Choose Wallet
                                            </p>

                                            <div className="grid grid-cols-1 gap-4 mt-5">
                                                <div
                                                    className="overflow-hidden transition-all duration-200 bg-white border border-gray-200 cursor-pointer rounded-xl hover:border-gray-400 hover:bg-gray-50"
                                                    onClick={signInWithMetamask}>
                                                    <div className="p-4">
                                                        <div className="flex items-center">
                                                            <img className="w-auto h-6 shrink-0" src="/assets/metamask.png" alt="" />
                                                            <p className="flex-1 ml-4 text-sm font-bold text-gray-900 text-left">
                                                                Metamask
                                                            </p>

                                                            {loading && (
                                                                <svg
                                                                    role="status"
                                                                    className="inline h-5 w-5 animate-spin text-gray-200"
                                                                    viewBox="0 0 100 101"
                                                                    fill="#000"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                >
                                                                    <path
                                                                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                                                                        fill="currentColor"
                                                                    />
                                                                    <path
                                                                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                                                        fill="currentFill"
                                                                    />
                                                                </svg>
                                                            )}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div
                                                    className="overflow-hidden transition-all duration-200 bg-white border border-gray-200 cursor-pointer opacity-60 rounded-xl hover:border-gray-400 hover:bg-gray-50">
                                                    <div className="p-4">
                                                        <div className="flex items-center">
                                                            <img className="w-auto h-6 shrink-0" src="/assets/wallet-connect.png" alt="" />
                                                            <p className="flex-1 ml-4 text-sm font-bold text-gray-900 text-left">
                                                                Wallet Connect
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    {message && <div className={`mt-6 text-center ${message.type === 'error' ? 'text-red-500' : 'text-green-500'}`}>
                                        {message.text}
                                    </div>}
                                    {!user?.currentUser && <div className="mt-6">
                                        <button type="button"
                                            className="relative inline-flex items-center justify-center w-full px-6 py-3 text-base font-medium text-white bg-gray-900 border border-transparent rounded-md shadow-sm hover:bg-gray-800 focus:outline-none focus:ring-0" onClick={signInWithDiscord}>
                                            <img className="w-auto h-8 shrink-0" src="/assets/discord.svg" alt="" />
                                            Sign in with Discord

                                            {loading && (
                                                <svg
                                                    role="status"
                                                    className="inline h-5 w-5 animate-spin text-gray-200 ml-4"
                                                    viewBox="0 0 100 101"
                                                    fill="#000"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                                                        fill="currentColor"
                                                    />
                                                    <path
                                                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                                        fill="currentFill"
                                                    />
                                                </svg>
                                            )}
                                        </button>
                                    </div>}
                                </div>
                            </motion.div>
                        </motion.div>
                    </motion.div>
                </>
            )
            }
        </AnimatePresence >
    );
}

export default ConnectWallet;