import React, { useState } from 'react';

const ComponentName = () => {
    return (
        <section className="py-12 bg-white sm:py-16 lg:py-20">
            <div className="px-4 mx-auto sm:px-6 lg:px-8 max-w-7xl">
                <div className="max-w-md mx-auto lg:max-w-6xl">
                    <div className="lg:flex">
                        <div className="h-auto lg:w-64 xl:w-80 shrink-0">
                            <img className="object-cover w-full h-auto mx-auto lg:mx-0 rounded-xl" src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/rarity/1/image.png" alt="" />

                            <div className="mt-5">
                                <a href="#" title=""
                                    className="inline-flex items-center justify-center w-full px-5 py-3 text-xs font-bold tracking-widest text-gray-500 uppercase transition-all duration-200 bg-transparent border border-gray-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-400 hover:bg-gray-100 hover:text-gray-900"
                                    role="button">
                                    View on Opensea
                                </a>
                            </div>
                        </div>

                        <div className="mt-8 lg:mt-0 lg:flex-1 lg:ml-12">
                            <div className="flex items-center justify-between">
                                <div>
                                    <p className="text-xl font-bold text-gray-900">
                                        Rarity Rank #3
                                    </p>
                                    <p className="mt-1 text-sm font-medium text-gray-500">
                                        ID: 41066
                                    </p>
                                </div>

                                <div className="text-right">
                                    <p className="text-xs font-bold tracking-wide text-gray-500 uppercase">
                                        Rarity Score
                                    </p>
                                    <p className="mt-1 text-lg font-bold text-indigo-600">
                                        2950
                                    </p>
                                </div>
                            </div>

                            <div className="grid grid-cols-2 gap-3 mt-6 sm:gap-4 lg:grid-cols-3 xl:grid-cols-4">
                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Background
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Orange
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +17.44
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    30
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Mouth
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Rage
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +21.91
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    94
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Hair
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Top Hat
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +37.49
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    146
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Facial Hair
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Big Beard
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +68.03
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    385
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Eyes
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Colorful Sunglass
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +89.01
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    542
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Flur
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Regular
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +7.35
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    43
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Mouth Prop
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Laces
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +14.25
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    58
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Hat
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Colorful Bandana
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +35.14
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    267
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Props
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    3 Arrows
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +155.23
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    391
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Body
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Slim
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +16.07
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    104
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Shirt
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Black Polo
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +55.10
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    219
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="bg-white border border-gray-200 rounded-md">
                                    <div className="p-3 sm:p-4">
                                        <div className="flex items-center justify-between space-x-2">
                                            <div>
                                                <p className="text-xs font-bold tracking-wide text-gray-400 uppercase">
                                                    Tattoo
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    Red Circle
                                                </p>
                                            </div>

                                            <div className="text-right">
                                                <p className="text-sm font-bold text-emerald-500">
                                                    +47.56
                                                </p>
                                                <p className="mt-1 text-sm font-bold text-gray-900">
                                                    105
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default ComponentName;